Feature: Functions Your logo
As store a user
i Would like to access the shopping site 
and search for a specific or any product.


Scenario: Search for a t-shirt
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "blouse"
    Then the result is displayed

Scenario Outline: Searche for anything
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "<item>"
    Then the result should contain "<expectedResult>"

    Examples:
    | item      | expectedResult | 
    | Blouse    | Blouse         | 
    | Short     | short          | 


Scenario: add to cart and continuo shopping
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "blouse"
    Then the result is displayed
    And user click in Add to Cart
    Then user click in Continuo shopping
    Then user is send the page product again 

Scenario: add to cart and Proceed to shopping
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "blouse"
    Then the result is displayed
    And user click in Add to Cart
    Then user click in Proceed to shopping
    Then user is send the shopping cart 
    When user see the cart shopping
    And Click in proceed to checkout


Scenario: add to cart and Create account
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "blouse"
    Then the result is displayed
    And user click in Add to Cart
    Then user click in Proceed to shopping
    Then user is send the shopping cart 
    When user see the cart shopping
    And Click in proceed to checkout
    When user inform email : "teste2505@teste.com"
    And Click Create an account
    Then User inform the personal information

 
Scenario: add to cart and create account with the same email
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "blouse"
    Then the result is displayed
    And user click in Add to Cart
    Then user click in Proceed to shopping
    Then user is send the shopping cart 
    When user see the cart shopping
    And Click in proceed to checkout
    When user inform email : "teste2505@teste.com"
    And Click Create an account
    Then User receive message about email already existing

Scenario: create account with wrong information
    Given user on home page of "http://automationpractice.com/index.php"
    When user search for "blouse"
    Then the result is displayed
    And user click in Add to Cart
    Then user click in Proceed to shopping
    Then user is send the shopping cart 
    When user see the cart shopping
    And Click in proceed to checkout
    When user inform email : "teste250595@teste.com"
    And Click Create an account
    And user inform wrong information
    Then user click in Register
    Then user receive a message about the wrong information
   

Scenario: Contact to Us
   Given user on homem page of "http://automationpractice.com/index.php"
   When user click in contact user
   Then the page contact to us is open
   And select Subject Heading 
   And user input the email
   And user input a message with "teste ciandt"
   Then user click send