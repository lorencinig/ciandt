package models;

public class User {
    public String id;
    public String userid;
    public String title;
    public String body;

    public User(){
    }

    public User(String Id, String userid, String title, String body){
        this.id = Id;
        this.userid = userid;
        this.title = title;
        this.body = body;
    }
}
