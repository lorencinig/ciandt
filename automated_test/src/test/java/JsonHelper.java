import io.restassured.response.Response;
import models.User;
import org.junit.Test;
import org.omg.CosNaming.NamingContextPackage.NotEmpty;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class JsonHelper extends TestBase {

    @Test
    public void JsonHelper_Get() {
        REQUEST.get("/users")
                .then()
                .body("name", NotNull)
        		.body("username", NotNull)
        		.body("email", NotNull);
        		                                         	
                
    }

    @Test
    public void JsonHelper_POST(){
        REQUEST.get("/users/1")
                .then()
               
                .and()
                    .body("id", equalTo(1))
                    .body("userid", equalTo(1))
                    .body("title", equalTo("TESTE DESAFIO"))
                    .body("body", equalTo("Testando desafio CiandT"));
    }
    @Test
    public void JsonHelper_POST_NOTitle(){
        REQUEST.get("/users/2")
                .then()
                .statusCode(400)
                .and()
                    .body("id", equalTo(2))
                    .body("userid", equalTo(2))
                    .body("title", equalTo(null))
                    .body("body", equalTo("Testando desafio CiandT sem titulo"));
    }
    
}
